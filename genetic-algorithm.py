# i will try a "hello world!" example for genetic algorithms
# evolve into the string "hello world!" from a random population of strings
# one char - ascii represented in 7 bits, "hello world!" == 12 chars
# therefore chromosome will be represented in 7 * 12 = 84 bits

from bitarray import bitarray
from operator import attrgetter
import random
import sys

class Individual:
	def __init__(self, bits):
		self.bits = bits
		self.fitness = self.fitness()
		self.nfitness = 0.0
		self.afitness = 0.0

	def fitness(self):
		f = 0
		i = 0
		while i < self.bits.length():
			cbits = self.bits[i:i+7]
			f += abs(int(cbits.to01(), 2) - ord(goal[i/7]))
			i = i + 7
		return f

	def normalise(self, sum_fitness):
		self.nfitness = self.fitness / float(sum_fitness)

	def accumulate(self, sum_prev):
		self.afitness = self.nfitness + sum_prev

	def __str__(self):
		string = ""
		i = 0
		while i < self.bits.length():
			cbits = self.bits[i:i+7]
			ii = int(cbits.to01(), 2)
			if ii < 0 or ii > 127:
				print "ASCII PROB: " + str(ii)
				sys.exit(1)
			string += chr(ii)
			i = i + 7
		return string

def population_fitness():
	sum_fitness = 0
	for i in population:
		sum_fitness += i.fitness
	return sum_fitness

def mutate():
	pass

def crossover():
	pass

# use "Hello World!" string as a goal
goal = "Hello World!"

# population of chromosomes (individuals)
pop_size = 100
population = []

for i in range(pop_size):
	population.append(Individual(bitarray(7 * 12)))

population = sorted(population, key=attrgetter('fitness'))
sum_fitness = population_fitness()

accumulated_fitness = 0.0
for i in population:
	i.normalise(sum_fitness)
	accumulated_fitness += i.nfitness
	i.accumulate(accumulated_fitness)

R = random.random()
selected_population = []
for i in population:
	if i.afitness > R:
		selected_population.append(i)
population = selected_population

for i in population:
	print "str: %s, f: %s, n: %s, a: %s" % (str(i), i.fitness, i.nfitness, i.afitness)

#a.append(True)
#a.extend([True, False, True, True])

#print bits
#print bits.length()

